Name:           megi-kernel-ostree
Version:        5.17
Release:        3%{?dist}
Summary:        latest megi kernel for the pinephone 

License:        GPLv3+
URL:            https://megous.com/git/linux/log/?h=orange-pi-5.17
Source0:        https://xff.cz/kernels/5.17/pp.tar.gz

Provides: kernel = %{version}-%{release}
Provides: kernel-core = %{version}-%{release}
Provides: kernel-modules = %{version}-%{release}
Provides: kernel-modules-extra = %{version}-%{release}

Provides: kernel-%{_arch} = %{version}-%{release}
Provides: kernel-core-%{_arch} = %{version}-%{release}
Provides: kernel-modules-%{_arch} = %{version}-%{release}
Provides: kernel-modules-extra-%{_arch} = %{version}-%{release}

Provides: kernel%{_isa} = %{version}-%{release}
Provides: kernel-core%{_isa} = %{version}-%{release}
Provides: kernel-modules%{_isa} = %{version}-%{release}
Provides: kernel-modules-extra%{_isa} = %{version}-%{release}

%description
Megis version of the Linux Kernel, for PinePhone.

%global debug_package %{nil}

%prep
%autosetup -p1 -n pp-5.17

%install
mkdir -p $RPM_BUILD_ROOT/lib/modules/
cp -r modules/lib/modules/* $RPM_BUILD_ROOT/lib/modules/
gzip -f9 Image
cp Image.gz $RPM_BUILD_ROOT/lib/modules/5.17.9-00620-gc654d072573f/vmlinuz
mkdir -p $RPM_BUILD_ROOT/lib/modules/5.17.9-00620-gc654d072573f/dtb/allwinner
cp board-1.1.dtb $RPM_BUILD_ROOT/lib/modules/5.17.9-00620-gc654d072573f/dtb/allwinner/sun50i-a64-pinephone-1.1.dtb
cp board-1.2.dtb $RPM_BUILD_ROOT/lib/modules/5.17.9-00620-gc654d072573f/dtb/allwinner/sun50i-a64-pinephone-1.2.dtb
cp pinetab.dtb $RPM_BUILD_ROOT/lib/modules/5.17.9-00620-gc654d072573f/dtb/allwinner/sun50i-a64-pinetab.dtb

%files
/lib/modules/

%changelog
* Sat Feb 05 2022 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.17-1
- Update to 5.17

* Tue Jan 11 2022 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.16-1
- Update to 5.15.6

* Mon Dec 13 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.15.6-1
- Update to 5.15.6

* Sat Oct 13 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.14.12-1
- Update to 5.14.12

* Tue Sep 28 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.14.8-1
- Update to 5.14.8

* Wed Sep 22 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.14.6-1
- Update to 5.14.6

* Fri Sep 10 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.14.1-1
- Update to 5.14.1

* Fri Sep 03 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.14-1
- Update to 5.14

* Sat Aug 21 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.13.12-1
- Update to 5.13.12

* Mon Jul 26 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.13.4-1
- Update to 5.13.4

* Mon Jul 12 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.13.1-1
- Update to 5.13

* Sat Jun 12 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.10-1
- update 

* Sun May 23 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.6-1
- Update

* Thu May 06 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.0-7
- udpate 

* Wed Apr 07 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.0-6
- Update 

* Wed Apr 07 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.0-5
- Update for pp-uboot dep

* Fri Apr 02 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.0-4
- Update to latest 5.12

* Mon Mar 22 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.0-3
- Update to latest 5.12

* Mon Mar 15 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.0-2
- Update 5.12 kernel rc

* Thu Mar 04 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.12.0-1
- Update to 5.12 kernel

* Fri Feb 26 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.11.0-5
- Update to latest 

* Mon Feb 15 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.11.0-4
- Update to latest 

* Sun Feb 14 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.11.0-3
- Update to latest

* Sun Feb 07 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.11.0-2
- Update to latest

* Sun Jan 31 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.11.0-1
- Update to latest

* Fri Jan 22 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.9-1
- Update to latest

* Tue Jan 12 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.4-2
- Don't require device specific packages (phone vs tablet)

* Fri Jan 01 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.4-1
- Upgrade to 5.10.4

* Thu Dec 03 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.rc6b-4
- Upgrade to 5.10-rc6b

* Tue Dec 01 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.rc6-3
- Upgrade to 5.10-rc6

* Sat Oct 31 2020 Nikhil Jha <hi@nikhiljha.com> - 5.10-2
- Upgrade to 5.10 as of oct31

* Fri Oct 16 2020 Yoda - 5.9.0 GA
- Upgrade to 5.9.0 GA
- New audio driver - requires new pineaudio util
- New kernel layout - requires new uboot

* Fri Aug 28 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.9-2
- Upgrade to 5.9 rc2

* Fri Aug 21 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.9-1
- Upgrade to 5.9

* Thu Aug 06 2020 Nikhil Jha <hi@nikhiljha.com> - 5.8-1
- Upgrade to 5.8

* Mon Jun 15 2020 Nikhil Jha <hi@nikhiljha.com> - 5.7-1
- Initial packaging (hack)
